<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/connect', function () {
    $query = http_build_query([
        'client_id' => '3',
        'redirect_uri' => 'http://laravel-passport.local/redirect',
        'response_type' => 'code',
        'scope' => 'write email',
    ]);

    return redirect('http://laravel-passport.local/oauth/authorize?'.$query);
});

Route::get('/redirect', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://laravel-passport.local/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '3',
            'client_secret' => '9Ze2bt13P5MSmSgmFmzLdweW7BM4r8wvpnlWnxZH',
            'redirect_uri' => 'http://laravel-passport.local/redirect',
            'code' => $request->query->get('code'),
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
});

Route::get('/connect-password-grant', function () {

    $http = new GuzzleHttp\Client;

    $response = $http->post('http://laravel-passport.local/oauth/token', [
        'form_params' => [
            'grant_type' => 'password',
            'client_id' => '2',
            'client_secret' => 'xIQoQPimqpdVXQiRU81wyRa78X2mnSxSY9CD38EC',
            'username' => 'mkpeacock@gmail.com',
            'password' => 'password',
            'scope' => 'write email',
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
});
